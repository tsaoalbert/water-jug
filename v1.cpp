#include <algorithm>
#include <set>
#include <stack>
#include <vector>
#include <iterator>
#include <iostream>
using namespace std;

template<
  template <class A,typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T*>& s ) {
  out << "{ ";
  for_each( cbegin(s), cend(s), [&out](auto c) {out << *c << " ";} );
  out << "}";
  return out;
}


template<
  template <class A,typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T>& s ) {
  out << "{ ";
  for_each( cbegin(s), cend(s), [&out](auto c) {out << c << " ";} );
  out << "}";
  return out;
}

class State {
  friend ostream& operator<< (ostream& out, const State& s ) {
    out << s.m_water << ", m_par=" << s.m_par ;
    return out;
  }

public:
  vector<int> m_water; // water in each Jug A,B,C
  int m_par;           // not worry for now

  // CSTR
  State (const initializer_list<int>& s): m_water{s}, m_par{ -1 } {};
  // State s{0,0,8};

  void pourWater ( size_t id, vector<State>&, int x, int y, size_t i, size_t j ) const ;

  // id: the id of the current state
  //  curr: current state
  // states: set of existing states 
  void pourWater ( size_t id, vector<State>& states, const vector<int>& capacity) const ;

  bool operator== ( const State& o ) const {
    return m_water == o.m_water;
  }
  bool contains ( int x ) const {
     return std::find(begin( m_water ), end( m_water ), x) != m_water.end() ;
  }

};
  // pourwater from Jug i to Jug j
  // X: size of Jug i
  // Y: size of Jug j
void State::pourWater ( size_t id, vector<State>& v, int X, int Y, size_t i, size_t j ) const {
  int x = m_water[i] ; 
  int y = m_water[j] ;
  State chd = *this ;  // duplicate the child state
  chd.m_water[i] = max (0, x-(Y-y) );
  chd.m_water[j] = min (Y, x+y );
  chd.m_par = id;
  cout << chd << endl;
}
/*
  id: the id of the parent state
  v: set of existing states
  cap: the capacity of each Jug.
*/
void State::pourWater ( size_t id, vector<State>& v, const vector<int>& cap) const {
  // TODO: fill in the missing pieces of codes
  size_t n = m_water.size();      // # of the water jugs
  for (size_t i=0; i < n; ++i ) {     //  for each pair of water jugs i and j
    for (size_t j=i+1; j < n; ++j ) { //  
      pourWater ( id, v, cap[i], cap[j], i, j ) ; // water from Jug i to j
      pourWater ( id, v, cap[j], cap[i], j, i ) ;  // water from Jug j to i
    }   
  }
}
class Master {
public:
  vector<int> m_capacity;
  vector< State > m_v;

  // CSTR
  Master (const initializer_list<int>& s): m_capacity{s}, m_v { State{s} }  {
    // TODO: fill in the missing pieces of codes
  };
  void spawn () ;

  void backtrace ( ) const {
  // TODO: fill in the missing pieces of codes
  }
};


void Master::spawn () { 
  // TODO: fill in the missing pieces of codes
}

void unitTestState () {
  cout << __PRETTY_FUNCTION__ << endl;
  vector<int> cap {3,5,8} ;
  State s0{0,0,8};
  vector<State> v { s0 } ;
  s0.pourWater (0, v, cap  ); 
}

void v1 () {
  //cout << g.m_capacity << endl;
  // g.spawn ();
  // g.backtrace();
}



