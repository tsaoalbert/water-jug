#include <algorithm>
#include <set>
#include <stack>
#include <vector>
#include <iterator>
#include <iostream>
using namespace std;


template<
  template <class A,typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T>& s ) {
  out << "{ ";
  for_each( cbegin(s), cend(s), [&out](auto c) {out << c << " ";} );
  out << "}";
  return out;
}

class State_v3 {
  friend ostream& operator<< (ostream& out, const shared_ptr<State_v3>& s ) {
    out << s->m_water ;
    return out;
  }
  friend ostream& operator<< (ostream& out, const State_v3& s ) {
    out << s.m_water ;
    return out;
  }

public:
  vector<int> m_water;
  shared_ptr<State_v3> m_par;
  // CSTR
  State_v3 (const vector<int>& s) ;

  bool operator== ( const State_v3& o ) const ;
  
  bool contains ( int x ) const {
    const auto& it = find(cbegin(m_water), cend(m_water), x) ; 
    return it!=cend(m_water) ;
  }
};

struct APtrComp
{
  bool operator()(const State_v3* x, const State_v3* y) const  { 
    return x->m_water < y->m_water ; 
  }
};

class Owner {
public:
  vector<int> m_capacity;
  vector< shared_ptr<State_v3> > m_v;
  set< const State_v3*, APtrComp > m_set;

  Owner (std::initializer_list<int> s) ;

  bool isNewState ( const State_v3* chd ) const {
    return m_set.find ( chd ) == cend(m_set) ;
  }


  virtual ~Owner () {
  }
  void spawn () ;

  // xxx to better organize the code
  // xxx move pourWater from class State_v3 to this class
  void pourWater ( size_t id, size_t i, size_t j ) ;
  void pourWater ( size_t id ) ;

  // find out the first state that measures amount-unit water
  auto findTargetState ( int amount ) const ;
  void backtrace ( int amount ) const ;

};
  


void v3 () {
  // Owner g {3,5,8, 20} ;
/*
  Owner g {4,7,13} ;
  cout << g.m_capacity << endl;
  g.spawn ();
  int n = g.m_capacity.back();
  for ( int i=1; i < n; ++i ) {
    g.backtrace( i );
  }
*/
}



