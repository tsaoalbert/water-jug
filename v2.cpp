#include <algorithm>
#include <set>
#include <stack>
#include <vector>
#include <iterator>
#include <iostream>
using namespace std;

template<
  template <class A,typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T*>& s ) {
  out << *s ;
  return out;
}


template<
  template <class A,typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T>& s ) {
  out << "{ ";
  for_each( cbegin(s), cend(s), [&out](auto c) {out << c << " ";} );
  out << "}";
  return out;
}

class State_v2 {
  friend ostream& operator<< (ostream& out, const State_v2& s ) {
    out << s.m_water ;
    return out;
  }

public:
  vector<int> m_water;
  const State_v2* m_par;
  // CSTR
  State_v2 (const vector<int>& s): m_water{s}, m_par{ nullptr } {};

  inline bool operator== ( const State_v2& o ) const ;

  inline bool contains ( int x ) const ;
};

struct APtrComp
{
  bool operator()(const State_v2* x, const State_v2* y) const  { 
    return x->m_water < y->m_water ; 
  }
};

class Transition {
public:
  vector<int> m_capacity;
  vector< State_v2* > m_v;
  set< const State_v2*, APtrComp > m_set;

  Transition (std::initializer_list<int> s);

  inline bool isNewState ( const State_v2* chd ) const ;

  virtual ~Transition () {
  }

  void spawn () ;

  // xxx to better organize the code
  // xxx move pourWater from class State_v2 to this class
  void pourWater ( size_t id, size_t i, size_t j ) ;
  void pourWater ( size_t id ) ;

  // find out the first state that measures amount-unit water
  const State_v2* findTargetState ( int amount ) const ;

  void backtrace ( int amount ) const ;
  
};


void v2 () {
/*
  Transition g {3,5,8} ;
  cout << g.m_capacity << endl;
  g.spawn ();
  for ( int i=1; i < 8; ++i ) {
    g.backtrace( i );
  }
*/
}



